import mongoose from 'mongoose';

//Define mongoose schema
const postSchema = mongoose.Schema({
    title:String,
    message:String,
    creator: String,
    tags: [String],
    selectedFile: String,
    likeCount: {
        type: Number,
        default: 0 
    },
    createAt: {
        type: Date,
        default: new Date()
    },
});

//Define model
const PostMessage = mongoose.model('PostMessage', postSchema);

export default PostMessage;