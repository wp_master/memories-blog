import express, { Router } from 'express';
import { getPosts, createPost, updatePost, deletePost, likePost } from '../controllers/posts.js';

//use express router
const router = express.Router();
//define routes
router.get('/', getPosts);
router.post('/', createPost);
router.patch('/:id', updatePost);
router.patch('/:id/likepost', likePost);
router.delete('/:id', deletePost);

export default router;