import mongoose from 'mongoose';
import PostMessage from '../models/postMessage.js';


export const getPosts = async (req, res)=>{

    try{
        //get all posts
        const postMessages = await PostMessage.find();
        //send OK request with post data
        res.status(200).json(postMessages);        


    }catch(error){

        res.status(404).json( { message:error.message} )
    }

};


export const createPost = async (req, res)=>{
    //new post body
    const post = req.body;
    //apply to model
    const newPost = new PostMessage(post);

    try{
        //save new post
        await newPost.save();
        //return status after save new post
        res.status(201).json(newPost);

    } catch (error) {
        //return error
        res.status(409).json({ message:error.message });
    }

}

export const updatePost = async(req, res) =>{
    //get the id
    const { id:_id } = req.params;

    //get the post data
    const post = req.body;
    //check id validation
    if(!mongoose.Types.ObjectId.isValid(_id)) return res.status(404).send('No posts with that id');
    
    //update post id
    const updatedPost = await PostMessage.findByIdAndUpdate(_id, {...post, _id}, { new:true });
    //send json reponse 
    res.json(updatedPost);
}

export const deletePost = async(req, res) =>{
    //get the id
    const { id } = req.params;
    //check id validation
    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No post with that id');
    //remove post id
    await PostMessage.findByIdAndRemove(id);

    res.json({ message: 'Post deleted successfully' });

}

export const likePost = async (req, res) =>{
    const { id } = req.params;

    //check id validation
    if(!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send('No post with that id');

    const post = await PostMessage.findById(id);
    const updatedPost = await PostMessage.findByIdAndUpdate(id, { likeCount: post.likeCount +1 }, { new: true });

    res.json(updatedPost);

}