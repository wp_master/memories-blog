
//import required packages
import express from 'express';
import bodyParser from 'body-parser';
import mongoose from 'mongoose';
import cors from 'cors';
//import routes
import postRoutes from './routes/posts.js';
import dotenv from 'dotenv';

const app = express();
dotenv.config();



//middlewares

//bodyParser configuration
app.use(bodyParser.json( {limit: "30mb", extended:true} ));
app.use(bodyParser.urlencoded( {limit: "30mb", extended:true} ));
//cors
app.use(cors());

//MongoDB
//const CONNECTION_URL = 'mongodb+srv://memories_admin_master:rleHF1HSSyowL0Yq@cluster0.cbvyc.mongodb.net/myFirstDatabase?retryWrites=true&w=majority';
const PORT = process.env.PORT || 5000;

//Mongoose initialize
mongoose.connect(process.env.CONNECTION_URL, {useNewUrlParser:true, useUnifiedTopology:true})
.then( ()=>{app.listen(PORT, ()=>console.log(`Server running on port: ${PORT}`))} )
.catch( (error)=>{console.log(error.message)} );

mongoose.set( 'useFindAndModify', false );

//App Routes
app.use('/posts', postRoutes);