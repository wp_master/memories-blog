import { combineReducers } from 'redux';
import posts from './posts';

//define all reducers
export default combineReducers({ posts });