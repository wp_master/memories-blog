import React, { useEffect, useState } from 'react';
import useStyles from './styles';
import { TextField, Button, Typography, Paper } from '@material-ui/core';
import FileBase from 'react-file-base64';
import { useDispatch, useSelector } from 'react-redux';
import { createPost , updatePost} from '../../actions/posts';

//Get the current id


const Form = ( { currentId, setCurrentId } ) => {

    //get css classess
    const classes = useStyles();

    //initial state for post data
    const [postData, setpostData] = useState({
        creator: '',
        title:'',
        message:'',
        tags:'',
        selectedFile:''
    });

    //get the id from data state
    const post = useSelector(state => currentId ? state.posts.find( (p)=> p._id === currentId) : null);

    useEffect(() => {

        if(post) setpostData(post);
        
    }, [post])


    const dispatch = useDispatch();

    //handle submit, check if to update post data or create new post
    const handleSubmit = (e) =>{

        e.preventDefault();

        if(currentId){

            dispatch(updatePost(currentId,postData));

        }else{

            //dispatch createPost action
            dispatch(createPost(postData));
        }

        //clear all fields after submit and update post
        clearFields();
        
       

    };

    //clear button
    const clearFields = () =>{
        //reset id
        setCurrentId(null);

        //reset postData to initial state
        setpostData({
            creator: '',
            title:'',
            message:'',
            tags:'',
            selectedFile:''
        });


    };


    

    return (
      <Paper className={classes.paper}>
      {/* Create new memory form*/ }
        <form autoComplete="off" noValidate className={`${classes.root} ${classes.form}`} onSubmit={handleSubmit}>
            <Typography variant="h6">{ currentId ? 'Editing...' : 'Share A New Memory' }</Typography>

            <TextField name="creator" 
            variant="outlined" label="Creator" fullWidth
            value={postData.creator}
            onChange={ (e)=> setpostData({...postData, creator:e.target.value })}
             />

            <TextField name="title" 
            variant="outlined" label="Memory Title" fullWidth
            value={postData.title}
            onChange={ (e)=> setpostData({...postData, title:e.target.value })}
             />

            <TextField name="message" 
            variant="outlined" label="Message" fullWidth
            value={postData.message}
            onChange={ (e)=> setpostData({...postData, message:e.target.value })}
             />

            <TextField name="tags" 
            variant="outlined" label="Add Tags" fullWidth
            value={postData.tags}
            onChange={ (e)=> setpostData({...postData, tags:e.target.value.split(',') })}
             />

             <div className={classes.fileInput}>
                <FileBase 
                    type="file"
                    multiple={false}
                    onDone={ ({base64}) => setpostData({...postData, selectedFile: base64})  }

                />
             </div>
             <Button className={classes.buttonSubmit} variant="contained" color="primary" side="large" type="submit" fullWidth>Submit</Button>
             <Button className={classes.buttonSubmit} variant="contained" color="secondary" side="small" onClick={clearFields} fullWidth>Clear</Button>


        </form>

      </Paper>
    )
}

export default Form
