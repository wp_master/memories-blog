import * as api from '../api';
import {CREATE, UPDATE, DELETE, FETCH_ALL} from '../constants/actionTypes';

//Actions Creators and Types

export const getPosts = () => async(dispatch) =>{
    
    try{
        //api call to fetch all data
        const { data } = await api.fetchPosts();
        //define dispatch type
        const action = { type: FETCH_ALL, payload: data };
        //dispatch the action after getting posts data
        dispatch(action);
    
    }catch(error){
    
        console.log(error.message);
    }
}


export const createPost =(post) => async(dispatch) =>{

    try{

        const { data } = await api.createPost(post);

        dispatch({type: CREATE, payload:data})

    }catch(error){
        
        console.log(error);
    }
};

export const updatePost = (id, post)=> async(dispatch)=>{

    try{

        const {data} = await api.updatePost(id, post);

        dispatch({ type: UPDATE, payload:data });
    }catch(error){
        console.log(error);
    }

}


export const deletePost = (id) => async(dispatch) => {

    try{
        //api call with post id to delte
        await api.deletePost(id);
        //dispatch delete action
        dispatch({ type: DELETE,payload: id });
        
    }catch(error){

        console.log(error);
    }

}

export const likePost = (id) => async (dispatch) => {

    try {

        const {data} = await api.likePost(id);

        dispatch({ type:UPDATE, payload:data });
        
    }catch (error) {
        console.log(error);
    }

}